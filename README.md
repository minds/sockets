# Minds Sockets

Sockets server for Minds.

### Building

```
npm run build && docker compose up sockets --build
```

## License

[AGPLv3](https://www.minds.org/docs/license.html). Please see the license file of each repository.

**_Copyright Minds 2024_**
