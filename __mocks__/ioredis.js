const Redis = jest.fn();

Redis.prototype = {
  __data: {},
  get data() {
    return this.__data;
  },
  set data(data) {
    this.__data = data;
  },
  get(key) {
    return this.data[key];
  },
  set(key, value) {
    this.data[key] = value;
  },
  duplicate() {
    return this;
  },
  connect() {
    return true;
  },
};

module.exports = {
  Redis,
};
