FROM node:18-alpine

RUN apk add --no-cache git

WORKDIR /var/minds/sockets

COPY . .

ENTRYPOINT node build/index.js