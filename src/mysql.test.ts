import * as mysql from "mysql2/promise";
import { getMySQLReplica } from "./mysql";

jest.mock("./config.ts", () => ({}));
jest.mock("mysql2/promise");

describe("mysql.ts", () => {
  test("it should connect to the replica", async () => {
    await getMySQLReplica();
  });
});
