import { readFileSync } from "node:fs";

export const PORT: number = parseInt(process.env["MINDS_SOCKET_PORT"]) || 3000;

export const REDIS_HOST = process.env["MINDS_REDIS_HOST"] || "redis";
export const REDIS_PORT = process.env["MINDS_REDIS_PORT"] || 6379;

export const MYSQL_HOST = process.env["MINDS_MYSQL_HOST"] || "mysql";
export const MYSQL_USER = process.env["MINDS_MYSQL_USER"] || "user";
export const MYSQL_PASSWORD = process.env["MINDS_MYSQL_PASSWORD"] || "changeme";
export const MYSQL_DB_NAME = process.env["MINDS_MYSQL_DB_NAME"] || "minds";

const JWT_SESSION_PUBKEY_FILENAME =
  process.env["MINDS_JWT_SESSION_PUBKEY_FILENAME"] || "/.dev/minds.pub";
const JWT_OAUTH_PUBKEY_FILENAME =
  process.env["MINDS_JWT_OAUTH_PUBKEY_FILENAME"] || "/.dev/minds.pub";

export const JWT_SESSION_PUBKEY = readFileSync(JWT_SESSION_PUBKEY_FILENAME);
export const JWT_OAUTH_PUBKEY = readFileSync(JWT_OAUTH_PUBKEY_FILENAME);
