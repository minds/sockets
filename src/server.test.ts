import { socketServer, rateLimiter } from "./server";
import { Server } from "socket.io";
import * as jwt from "jsonwebtoken";
import * as memory from "./memory";
import * as chat from "./chat";
import { RateLimiterMemory, RateLimiterRes } from "rate-limiter-flexible";

jest.mock("../src/config", () => ({
  JWT_OAUTH_PUBKEY: "jwt-auth-key",
  JWT_SESSION_PUBKEY: "jwt-session-key",
  REDIS_PORT: 6987,
}));
jest.mock("./memory");
jest.mock("./mysql");
jest.mock("./chat");

jest.mock("@socket.io/redis-adapter");
jest.genMockFromModule("socket.io");
jest.mock("socket.io");
jest.mock("jsonwebtoken");

jest.mock("rate-limiter-flexible", () => {
  return {
    RateLimiterMemory: jest.fn().mockImplementation(() => {
      return {
        consume: jest.fn().mockResolvedValue({} as RateLimiterRes),
      };
    }),
  };
});

describe("server.ts", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  test("it should load the server", () => {
    socketServer();
  });

  test("it should allow auth from access token", async () => {
    const socket = {
      handshake: {
        auth: {
          accessToken: "fake token",
        },
        address: "::ffff:172.20.0.12",
      },
    };

    const next = jest.fn();

    jest.spyOn(jwt, "verify").mockReturnValue({
      sub: "123",
    } as any);

    const ioMock = {
      use: (cb) => cb(socket, next),
      on: jest.fn(),
    };

    (Server as any).mockImplementation(() => ioMock);

    await socketServer();

    expect(jwt.verify).toHaveBeenCalledWith("fake token", "jwt-auth-key");
    expect(next).toHaveBeenCalled();
    expect(memory.addUser).toHaveBeenCalledWith(BigInt(123), socket);
  });

  test("it not should allow auth from invalid access token", async () => {
    const socket = {
      handshake: {
        auth: {
          accessToken: "fake token",
        },
        address: "::ffff:172.20.0.12",
      },
    };

    const next = jest.fn();

    jest.spyOn(jwt, "verify").mockImplementationOnce(() => {
      throw new Error();
    });

    const ioMock = {
      use: (cb) => cb(socket, next),
      on: jest.fn(),
    };

    (Server as any).mockImplementation(() => ioMock);

    await socketServer();

    expect(jwt.verify).toHaveBeenCalledWith("fake token", "jwt-auth-key");
    expect(next).not.toHaveBeenCalled();
    expect(memory.addUser).not.toHaveBeenCalled();
  });

  test("it should allow auth from a session cookie", async () => {
    const socket = {
      handshake: {
        headers: {
          cookie: "minds_sess=fake token;",
        },
        address: "::ffff:172.20.0.12",
      },
    };

    const next = jest.fn();

    jest.spyOn(jwt, "verify").mockReturnValue({
      sub: "123",
    } as any);

    jest.spyOn(memory, "addUser");

    const ioMock = {
      use: (cb) => cb(socket, next),
      on: jest.fn(),
    };

    (Server as any).mockImplementation(() => ioMock);

    await socketServer();

    expect(jwt.verify).toHaveBeenCalledWith("fake token", "jwt-session-key");
    expect(next).toHaveBeenCalled();
    expect(memory.addUser).toHaveBeenCalledWith(BigInt(123), socket);
  });

  test("it should not allow auth from a invalid session cookie", async () => {
    const socket = {
      handshake: {
        headers: {
          cookie: "minds_sess=fake token;",
        },
        address: "::ffff:172.20.0.12",
      },
    };

    const next = jest.fn();

    jest.spyOn(jwt, "verify").mockImplementationOnce(() => {
      throw new Error();
    });

    const ioMock = {
      use: (cb) => cb(socket, next),
      on: jest.fn(),
    };

    (Server as any).mockImplementation(() => ioMock);

    await socketServer();

    expect(jwt.verify).toHaveBeenCalledWith("fake token", "jwt-session-key");
    expect(next).not.toHaveBeenCalled();
    expect(memory.addUser).not.toHaveBeenCalled();
  });

  test("it should allow the client to join on legacy rooms", async () => {
    const socket = {
      id: "socket-id",
      join: jest.fn(),
      on: (eventName, cb) => {
        if (eventName === "join") {
          cb("entity:metrics:123");
          cb("marker:123");
          cb("comments:123");
          cb("moderation_summon");
          cb("notification:count:123");

          // Not to allow
          cb("chat:123");
          cb("notification:count:456");
        }
      },
      to: (roomName) => ({ emit: jest.fn() }),
      emit: jest.fn(),
      handshake: {
        address: "::ffff:172.20.0.12",
      },
    };

    jest.spyOn(chat, "getChatRoomGuids").mockReturnValue(Promise.resolve([]));

    memory.socketIdToUserGuid.set("socket-id", 123);

    const ioMock = {
      use: jest.fn(),
      on: (eventName, cb) => cb(socket),
      to: (roomName) => ({ emit: jest.fn() }),
    };

    (Server as any).mockImplementation(() => ioMock);

    await socketServer();

    expect(socket.join).toHaveBeenCalledWith("entity:metrics:123");
    expect(socket.join).toHaveBeenCalledWith("marker:123");
    expect(socket.join).toHaveBeenCalledWith("comments:123");
    expect(socket.join).toHaveBeenCalledWith("moderation_summon");
    expect(socket.join).toHaveBeenCalledWith("notification:count:123");

    expect(socket.join).not.toHaveBeenCalledWith("chat:123");
    expect(socket.join).not.toHaveBeenCalledWith("notification:count:456");
  });

  test("it should unregister the socket to user guid map on disconnect", async () => {
    const socket = {
      id: "socket-id",
      join: jest.fn(),
      on: (eventName, cb) => {
        if (eventName === "disconnect") {
          cb("Just testing");
        }
      },
      to: (roomName) => ({ emit: jest.fn() }),
      emit: jest.fn(),
      handshake: {
        address: "::ffff:172.20.0.12",
      },
    };

    jest.spyOn(chat, "getChatRoomGuids").mockReturnValue(Promise.resolve([]));

    memory.socketIdToUserGuid.set("socket-id", 123);

    const ioMock = {
      use: jest.fn(),
      on: (eventName, cb) => cb(socket),
      to: (roomName) => ({ emit: jest.fn() }),
    };

    (Server as any).mockImplementation(() => ioMock);

    await socketServer();

    expect(memory.deleteUserBySocket).toHaveBeenCalledWith(socket);
  });

  test("it should join available chat rooms on connection", async () => {
    const socket = {
      id: "socket-id",
      join: jest.fn(),
      on: (eventName, cb) => {
        return;
      },
      to: (roomName) => ({ emit: jest.fn() }),
      emit: jest.fn(),
      handshake: {
        address: "::ffff:172.20.0.12",
      },
    };

    jest
      .spyOn(chat, "getChatRoomGuids")
      .mockReturnValue(Promise.resolve([123, 456]));

    memory.socketIdToUserGuid.set("socket-id", 123);

    const ioMock = {
      use: jest.fn(),
      on: (eventName, cb) => cb(socket),
      to: (roomName) => ({ emit: jest.fn() }),
    };

    (Server as any).mockImplementation(() => ioMock);

    await socketServer();

    // Wait for all promises to resolve
    await new Promise(process.nextTick);

    expect(socket.join).toHaveBeenCalledWith(["chat:123", "chat:456"]);
    expect(rateLimiter.consume).toHaveBeenCalledWith("::ffff:172.20.0.12");
  });

  test("it should join available chat rooms when refresh called", async () => {
    const socket = {
      id: "socket-id",
      join: jest.fn(),
      on: (eventName, cb) => {
        if (eventName === "chat_refresh_rooms") {
          cb();
        }
      },
      to: (roomName) => ({ emit: jest.fn() }),
      emit: jest.fn(),
      handshake: {
        address: "::ffff:172.20.0.12",
      },
    };

    jest
      .spyOn(chat, "getChatRoomGuids")
      .mockReturnValue(Promise.resolve([123, 456]));

    jest.spyOn(socket, "to");

    memory.socketIdToUserGuid.set("socket-id", 123);

    const ioMock = {
      use: jest.fn(),
      on: (eventName, cb) => cb(socket),
      to: (roomName) => ({ emit: jest.fn() }),
    };

    jest.spyOn(ioMock, "to");

    jest.spyOn(rateLimiter, "consume").mockResolvedValue({} as RateLimiterRes);

    (Server as any).mockImplementation(() => ioMock);

    await socketServer();

    // Wait for all promises to resolve
    await new Promise(process.nextTick);

    expect(rateLimiter.consume).toHaveBeenCalledWith("::ffff:172.20.0.12");
    expect(socket.join).toHaveBeenCalledWith(["chat:123", "chat:456"]);
    expect(socket.join).toHaveBeenCalledTimes(3);
    expect(ioMock.to).toHaveBeenCalledWith("user:123");
  });

  test("it should not join available chat rooms if rate limited", async () => {
    const socket = {
      id: "socket-id",
      join: jest.fn(),
      on: (eventName, cb) => {
        if (eventName === "chat_refresh_rooms") {
          cb();
        }
      },
      to: (roomName) => ({ emit: jest.fn() }),
      emit: jest.fn(),
      handshake: {
        address: "::ffff:172.20.0.12",
      },
    };

    jest
      .spyOn(chat, "getChatRoomGuids")
      .mockReturnValue(Promise.resolve([123, 456]));

    jest.spyOn(socket, "to");

    memory.socketIdToUserGuid.set("socket-id", 123);

    const ioMock = {
      use: jest.fn(),
      on: (eventName, cb) => cb(socket),
      to: (roomName) => ({ emit: jest.fn() }),
    };

    jest.spyOn(ioMock, "to");

    jest.spyOn(rateLimiter, "consume").mockImplementation(() => {
      throw new Error("Rate limited");
    });

    (Server as any).mockImplementation(() => ioMock);

    await socketServer();

    // Wait for all promises to resolve
    await new Promise(process.nextTick);

    expect(rateLimiter.consume).toHaveBeenCalledWith("::ffff:172.20.0.12");
    expect(chat.getChatRoomGuids).not.toHaveBeenCalled();
    expect(socket.join).not.toHaveBeenCalledWith(["chat:123", "chat:456"]);
    expect(socket.join).not.toHaveBeenCalledTimes(3);
    expect(ioMock.to).not.toHaveBeenCalledWith("user:123");
  });
});
