import {
  addUser,
  deleteUserBySocket,
  socketIdToUserGuid,
  userGuidToSocket,
} from "./memory";

describe("memory.test", () => {
  test("it should add a user", () => {
    addUser(123, { id: "socket-id" } as any);

    expect(socketIdToUserGuid.get("socket-id")).toBe(123);
    expect(userGuidToSocket.get(123).id).toBe("socket-id");
  });

  test("it should remove a socket", () => {
    addUser(123, { id: "socket-id" } as any);

    const result = deleteUserBySocket({ id: "socket-id" } as any);

    expect(result).toBe(true);

    expect(socketIdToUserGuid["socket-id"]).toBe(undefined);
    expect(userGuidToSocket[123]).toBe(undefined);
  });
});
