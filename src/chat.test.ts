import { getChatRoomGuids } from "./chat";
import * as mysql from "./mysql";

jest.mock("./config", () => ({}));
jest.mock("./mysql", () => ({
  getMySQLReplica: async () => ({
    query: jest.fn(),
  }),
}));

describe("chat.ts", () => {
  test("it should return a list of chat room guids", async () => {
    jest.spyOn(mysql, "getMySQLReplica").mockResolvedValue({
      query: async (sql) => {
        return [
          [
            {
              room_guid: 123,
            },
          ],
        ];
      },
    } as any);

    const result = await getChatRoomGuids(123);

    expect(result).toEqual([123]);
  });
});
