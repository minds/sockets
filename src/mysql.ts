import * as mysql from "mysql2/promise";
import {
  MYSQL_DB_NAME,
  MYSQL_HOST,
  MYSQL_PASSWORD,
  MYSQL_USER,
} from "./config";

let mysqlReplica: mysql.Connection;

/**
 * Returns a connection to the MySQL replica
 */
export const getMySQLReplica = async () => {
  if (mysqlReplica) {
    return mysqlReplica;
  }

  mysqlReplica = await mysql.createConnection({
    host: MYSQL_HOST,
    user: MYSQL_USER,
    password: MYSQL_PASSWORD,
    database: MYSQL_DB_NAME,
    supportBigNumbers: true,
  });

  console.log("Connected to MySQL");

  return mysqlReplica;
};
