import { getMySQLReplica } from "./mysql";

/**
 * Returns a list of chat rooms that a user is a member of
 */
export const getChatRoomGuids = async (userGuid: number): Promise<number[]> => {
  const mysqlReplica = await getMySQLReplica();

  const [results] = await mysqlReplica.query(
    `
        SELECT membership.room_guid
        FROM (
            SELECT tenant_id, room_guid, member_guid, status, role_id, joined_timestamp
            FROM minds_chat_members
            UNION
            SELECT tenant_id,
                  room_guid,
                  user_guid AS member_guid,
                  "ACTIVE" AS status,
                  CASE WHEN gm.membership_level >= 3 THEN "OWNER" ELSE "MEMBER" END AS role_id,
                  gm.created_timestamp AS joined_timestamp
            FROM minds_chat_rooms as r
            INNER JOIN minds_group_membership as gm ON (r.group_guid = gm.group_guid)
          ) as membership
          WHERE membership.member_guid = ?
          AND membership.status = 'ACTIVE'
        `,
    [userGuid],
  );

  const roomGuids = (results as any).map((row) => row.room_guid);

  return roomGuids;
};
