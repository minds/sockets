import { Server } from "socket.io";
import { Redis } from "ioredis";
import { createAdapter } from "@socket.io/redis-adapter";

import {
  JWT_OAUTH_PUBKEY,
  JWT_SESSION_PUBKEY,
  PORT,
  REDIS_HOST,
  REDIS_PORT,
} from "./config";
import * as jwt from "jsonwebtoken";
import * as cookie from "cookie";
import { addUser, deleteUserBySocket, socketIdToUserGuid } from "./memory";
import { getMySQLReplica } from "./mysql";
import { getChatRoomGuids } from "./chat";
import { RateLimiterMemory } from "rate-limiter-flexible";

export const rateLimiter = new RateLimiterMemory({
  points: 2, // 2 points
  duration: 1, // per second
});

export const socketServer = async () => {
  console.log("Starting sockets server");

  /**
   * Connect to MySQL
   */
  await getMySQLReplica();

  /**
   * Connect to Redis PubSub
   */
  const pubClient = new Redis({
    host: REDIS_HOST,
    port: REDIS_PORT as number,
  });
  const subClient = pubClient.duplicate();

  console.log("Connected to Redis (pubsub)");

  /**
   * Start the Socket.io server
   */
  const io = new Server(PORT, {
    path: "/api/sockets/socket.io",
    adapter: createAdapter(pubClient, subClient),
  });

  /**
   * Authentication middlware
   */
  io.use((socket, next) => {
    /**
     * If an access token is provided, attempt to use this first
     */
    const token = socket.handshake.auth?.accessToken;

    if (token) {
      // Validate the OAuth token
      try {
        const payload = jwt.verify(token, JWT_OAUTH_PUBKEY) as { sub: string };

        const userGuid = payload.sub;

        addUser(BigInt(userGuid), socket);

        console.debug("[Authenticated][OAuth]: " + userGuid);

        next();
      } catch (err) {
        console.error(err);
        return;
      }

      return;
    }

    /**
     * If no token provided, look for cookie based session atuh
     */
    // Is there a `minds_sess` cookie
    const headers = socket.handshake.headers;
    const cookieHeader = headers.cookie;

    try {
      const cookies = cookie.parse(cookieHeader);

      if (cookies["minds_sess"]) {
        const payload = jwt.verify(cookies["minds_sess"], JWT_SESSION_PUBKEY);

        const userGuid = payload["sub"] || payload["user_guid"];

        addUser(BigInt(userGuid), socket);

        console.debug("[Authenticated][Session]: " + userGuid);

        next();
      }
    } catch (err) {
      console.error(err);
    }
  });

  /**
   * Listen to socket connections (post authentication)
   */
  io.on("connection", async (socket) => {
    try {
      const userGuid = socketIdToUserGuid.get(socket.id);

      /**
       * Join the user to their own room so we can emit directly to the user
       */
      socket.join("user:" + userGuid);

      /**
       * Get a list of chat rooms and join the user to them
       */

      const joinChatRooms = async () => {
        try {
          console.log(socket.handshake.address);
          await rateLimiter.consume(socket.handshake.address); // consume 1 point per event from IP
        } catch (err) {
          console.info(`${socket.handshake.address} hit rate limit`);
          return;
        }

        const chatRoomGuids = await getChatRoomGuids(userGuid);
        socket.join(chatRoomGuids.map((roomGuid) => "chat:" + roomGuid));

        // Emit the room list to the client too, so they can track
        io.to("user:" + userGuid).emit("chat_rooms", chatRoomGuids);
      };
      joinChatRooms();

      /**
       * Support for global rooms
       */
      socket.on("join", (roomName: string) => {
        const allowedRoomPrefixes = [
          "entity:metrics:",
          "marker:",
          "comments:",
          "moderation_summon",
          "notification:count:" + userGuid,
          "tenant:bootstrap:",
        ];

        for (const prefix of allowedRoomPrefixes) {
          if (roomName.indexOf(prefix) > -1) {
            socket.join(roomName);
            console.debug(`[Room Joined]: ${userGuid} <-> ${roomName}`);
          }
        }
      });

      /**
       * Refresh the chat rooms a socket is subscribed to
       */
      socket.on("chat_refresh_rooms", () => joinChatRooms());

      /**
       * Remove the socket id mapping when the user goes way
       */
      socket.on("disconnect", (reason) => {
        const userGuid = socketIdToUserGuid.get(socket.id);

        deleteUserBySocket(socket);

        console.debug("[Disconnected]: " + userGuid + " " + reason);
      });
    } catch (err) {
      // Should we emit back connection errors?
      console.error(err);
    }
  });

  console.log("Socket server is running on port " + PORT);
};

export default socketServer;
