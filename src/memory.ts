import { Socket } from "socket.io";

export const socketIdToUserGuid = new Map<string, number>();
export const userGuidToSocket = new Map<number, Socket>();

/**
 * Maps a user to socket
 */
export const addUser = (userGuid: BigInt | number, socket: Socket) => {
  socketIdToUserGuid.set(socket.id, userGuid as number);
  userGuidToSocket.set(userGuid as number, socket);
};

/**
 * Removes the mapping of a user to a socket
 */
export const deleteUserBySocket = (socket: Socket): boolean => {
  const userGuid = socketIdToUserGuid.get(socket.id);
  return (
    socketIdToUserGuid.delete(socket.id) && userGuidToSocket.delete(userGuid)
  );
};
