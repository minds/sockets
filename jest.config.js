module.exports = {
  preset: "ts-jest",
  transform: {
    "^.+\\.(ts|tsx)?$": "ts-jest",
  },
  testPathIgnorePatterns: ["/node_modules/", ".yarn"],
  setupFilesAfterEnv: ["<rootDir>/jest/setupTests.js"],
  roots: ["<rootDir>/src", "<rootDir>/__mocks__"],
};
